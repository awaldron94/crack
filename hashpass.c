#include <stdio.h>
#include "md5.h"
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    FILE *src = fopen(argv[1], "r");
    FILE *hashed = fopen(argv[2], "w");
    if(!src)
    {
        printf("can't open %s for reading\n", argv[1]);
        exit(1);
    }
    if(!hashed)
    {
        printf("can't open %s for reading\n", argv[2]);
        exit(1);
    }
    char line[100];
    
    while(fgets(line,100,src)!= NULL)
    {
        line[strlen(line)-1] = '\0';
        char *hash = md5(line,strlen(line));
        fprintf(hashed, "%s\n", hash);
        free(hash);
    }
    fclose(src);
    fclose(hashed);
}